package service

import (
	"api-gateway/bookpb"
	"api-gateway/entity"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Service struct {
	BookService BookServiceClient
}

func New(bookServiceURL string) Service {
	conn, err := grpc.Dial(bookServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	return Service{
		BookService: bookServiceAdapter{
			client: bookpb.NewBookServiceClient(conn),
		},
	}
}

type BookServiceClient interface {
	RegisterBook(ctx context.Context, req entity.RegisterBookRequest) (entity.Book, error)
	GetBook(ctx context.Context, req string) (entity.Book, error)
	UpdateBook(ctx context.Context, req entity.UpdateBookRequest) (entity.Book, error)
	DeleteBook(ctx context.Context, req string) error
	GetListBook(ctx context.Context) ([]entity.Book, error)

	CreateAuthor(ctx context.Context, request string) (entity.Author, error)
}

type bookServiceAdapter struct {
	client bookpb.BookServiceClient
}

func (b bookServiceAdapter) GetListBook(ctx context.Context) ([]entity.Book, error) {
	books, err := b.client.GetListBooks(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	return fromProtoBooksToEntityBooks(books), nil
}

func (b bookServiceAdapter) DeleteBook(ctx context.Context, request string) error {
	_, err := b.client.DeleteBook(ctx, &bookpb.DeleteBookRequest{Id: request})
	return err
}

func (b bookServiceAdapter) UpdateBook(ctx context.Context, request entity.UpdateBookRequest) (entity.Book, error) {
	grpcRequest := &bookpb.UpdateBookRequest{
		Id:       request.ID,
		Name:     request.Name,
		AuthorId: request.AuthorID,
	}
	response, err := b.client.UpdateBook(ctx, grpcRequest)
	if err != nil {
		return entity.Book{}, err
	}

	return entity.Book{
		ID:       response.Id,
		Name:     response.Name,
		AuthorId: response.AuthorId,
	}, nil
}

func (b bookServiceAdapter) GetBook(ctx context.Context, request string) (entity.Book, error) {
	response, err := b.client.GetBook(ctx, &bookpb.GetBookRequest{Id: request})
	if err != nil {
		return entity.Book{}, err
	}

	return entity.Book{
		ID:       response.Id,
		Name:     response.Name,
		AuthorId: response.AuthorId,
	}, nil
}

func (b bookServiceAdapter) RegisterBook(ctx context.Context, request entity.RegisterBookRequest) (entity.Book, error) {
	grpcRequest := &bookpb.RegisterBookRequest{
		Name:     request.Name,
		AuthorId: request.AuthorID,
	}
	response, err := b.client.RegisterBook(ctx, grpcRequest)
	if err != nil {
		return entity.Book{}, err
	}

	return entity.Book{
		ID:       response.Id,
		Name:     response.Name,
		AuthorId: response.AuthorId,
	}, nil
}

func (b bookServiceAdapter) CreateAuthor(ctx context.Context, request string) (entity.Author, error) {
	response, err := b.client.CreateAuthor(ctx, &bookpb.CreateAuthorRequest{Name: request})
	if err != nil {
		return entity.Author{}, err
	}

	return entity.Author{
		ID:   response.Id,
		Name: response.Name,
	}, nil
}

func fromProtoBooksToEntityBooks(booksProto *bookpb.Books) []entity.Book {
	var books []entity.Book
	for _, b := range booksProto.Books {
		books = append(books, entity.Book{
			ID:       b.GetId(),
			Name:     b.GetName(),
			AuthorId: b.GetAuthorId(),
		})
	}
	return books
}
