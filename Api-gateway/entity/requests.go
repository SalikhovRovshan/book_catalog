package entity

type RegisterBookRequest struct {
	Name     string `json:"name"`
	AuthorID string `json:"author_id"`
}

type UpdateBookRequest struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	AuthorID string `json:"author_id"`
}
