package entity

type Book struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	AuthorId string `json:"author_id"`
}

type Author struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
