package service

import (
	"book_service/domain/author"
	"book_service/domain/book"
	"context"
	"github.com/google/uuid"
)

type Service struct {
	repo          Repository
	bookFactory   book.Factory
	authorFactory author.Factory
}

func New(repo Repository, bookfactory book.Factory, authorfactory author.Factory) Service {
	return Service{
		repo:          repo,
		bookFactory:   bookfactory,
		authorFactory: authorfactory,
	}
}

func (s Service) RegisterBook(ctx context.Context, b book.Book) (book.Book, error) {
	if err := s.repo.CreateBook(ctx, b); err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) CreateAuthor(ctx context.Context, a author.Author) (author.Author, error) {
	if err := s.repo.CreateAuthor(ctx, a); err != nil {
		return author.Author{}, err
	}
	return a, nil
}

func (s Service) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := s.repo.GetBook(ctx, id)
	if err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) UpdateBook(ctx context.Context, b book.Book) (book.Book, error) {
	b, err := s.repo.UpdateBook(ctx, b)
	if err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) DeleteBook(ctx context.Context, id uuid.UUID) error {
	err := s.repo.DeleteBook(ctx, id)
	return err
}

func (s Service) GetListBooks(ctx context.Context) ([]book.Book, error) {
	books, err := s.repo.GetListBook(ctx)
	if err != nil {
		return nil, err
	}
	return books, nil
}
