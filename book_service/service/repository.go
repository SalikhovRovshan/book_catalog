package service

import (
	"book_service/domain/author"
	"book_service/domain/book"
	"context"
	"github.com/google/uuid"
)

type Repository interface {
	BookRepository
	AuthorRepository
}

type BookRepository interface {
	CreateBook(ctx context.Context, b book.Book) error
	GetBook(ctx context.Context, id uuid.UUID) (book.Book, error)
	UpdateBook(ctx context.Context, b book.Book) (book.Book, error)
	DeleteBook(ctx context.Context, id uuid.UUID) error
	GetListBook(ctx context.Context) ([]book.Book, error)
}

type AuthorRepository interface {
	CreateAuthor(ctx context.Context, a author.Author) error
}
