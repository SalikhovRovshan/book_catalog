package server

import (
	"book_service/bookpb"
	"book_service/domain/author"
	"book_service/domain/book"
	"book_service/pkg/converters"
	"book_service/service"
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	bookpb.UnimplementedBookServiceServer
	service       service.Service
	bookFactory   book.Factory
	authorFactory author.Factory
}

func New(svc service.Service, bookFactory book.Factory, authorFactory author.Factory) Server {
	return Server{
		service:       svc,
		bookFactory:   bookFactory,
		authorFactory: authorFactory,
	}
}

func (s Server) RegisterBook(ctx context.Context, request *bookpb.RegisterBookRequest) (*bookpb.Book, error) {
	book, err := s.convertRegisterBookRequestToDomainBook(request)
	if err != nil {
		return nil, err
	}

	createdBook, err := s.service.RegisterBook(ctx, book)
	if err != nil {
		return nil, err
	}
	return converters.ToProtoBook(createdBook), nil
}

func (s Server) CreateAuthor(ctx context.Context, request *bookpb.CreateAuthorRequest) (*bookpb.Author, error) {
	author, err := s.authorFactory.NewAuthor(request.Name)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	createdAuthor, err := s.service.CreateAuthor(ctx, author)
	if err != nil {
		return nil, status.Error(codes.Canceled, err.Error())
	}
	return converters.ToProtoAuthor(createdAuthor), nil
}

func (s Server) GetBook(ctx context.Context, request *bookpb.GetBookRequest) (*bookpb.Book, error) {
	id, err := uuid.Parse(request.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	b, err := s.service.GetBook(ctx, id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return converters.ToProtoBook(b), nil
}

func (s Server) UpdateBook(ctx context.Context, request *bookpb.UpdateBookRequest) (*bookpb.Book, error) {
	book, err := s.convertUpdateBookRequestToDomainBook(request)
	if err != nil {
		return nil, err
	}

	updatedBook, err := s.service.UpdateBook(ctx, book)
	if err != nil {
		return nil, err
	}
	return converters.ToProtoBook(updatedBook), nil
}

func (s Server) DeleteBook(ctx context.Context, id *bookpb.DeleteBookRequest) (*emptypb.Empty, error) {
	bookId, err := uuid.Parse(id.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	err = s.service.DeleteBook(ctx, bookId)
	return &emptypb.Empty{}, err
}

func (s Server) GetListBooks(ctx context.Context, empty *emptypb.Empty) (*bookpb.Books, error) {
	books, err := s.service.GetListBooks(ctx)
	if err != nil {
		status.Error(codes.Internal, err.Error())
	}
	return converters.ToProtoBooks(books), nil
}

func (s Server) convertUpdateBookRequestToDomainBook(protoBook *bookpb.UpdateBookRequest) (book.Book, error) {
	authorID, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided author id is not uuid")
	}

	bookId, err := uuid.Parse(protoBook.GetId())
	if err != nil {
		return book.Book{}, err
	}

	unmarshaledBook := book.UnmarshalBookArgs{
		ID:       bookId,
		Name:     protoBook.Name,
		AuthorId: authorID,
	}

	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return book.UnmarshalBook(unmarshaledBook)
}

func (s Server) convertRegisterBookRequestToDomainBook(protoBook *bookpb.RegisterBookRequest) (book.Book, error) {
	authorID, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided author id is not uuid")
	}

	b, err := s.bookFactory.NewBook(
		protoBook.Name,
		authorID,
	)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return b, nil
}
