package main

import (
	"book_service/bookpb"
	"book_service/config"
	"book_service/domain/author"
	"book_service/domain/book"
	"book_service/pkg/id"
	repository "book_service/repository"
	"book_service/server"
	"book_service/service"
	"errors"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Fatal("err while loading config")
	}

	repo, err := repository.NewPostgres(cfg.PostgresConfig)
	if err != nil {
		log.Fatal("err while creating Postgres structure")
	}

	authorFactory := author.NewFactory(id.Generator{})
	bookFactory := book.NewFactory(id.Generator{})

	svc := service.New(repo, bookFactory, authorFactory)
	server := server.New(svc, bookFactory, authorFactory)

	lis, err := net.Listen("tcp", net.JoinHostPort(cfg.Host, cfg.Port))
	if err != nil {
		fmt.Println(errors.New("error in JoinHostPort"))
		return
	}

	grpcServer := grpc.NewServer()

	bookpb.RegisterBookServiceServer(grpcServer, server)

	if err = grpcServer.Serve(lis); err != nil {
		fmt.Println(errors.New("error while serving grpc"))
		return
	}
}
