package repository

import (
	"book_service/config"
	"book_service/domain/author"
	"book_service/domain/book"
	"book_service/pkg/converters"
	"context"
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/jmoiron/sqlx"
	"log"
)

type Postgres struct {
	db *sqlx.DB
}

func NewPostgres(config config.Postgresconfig) (*Postgres, error) {
	db, err := connect(config)
	if err != nil {
		log.Fatal(err)
		return &Postgres{}, err
	}

	return &Postgres{
		db: db,
	}, nil
}

func (p *Postgres) GetListBook(ctx context.Context) ([]book.Book, error) {
	return p.getListBook(ctx)
}

func (p *Postgres) getListBook(ctx context.Context) ([]book.Book, error) {
	query := `select * from books`
	books := make([]converters.Book, 0)

	if err := p.db.SelectContext(ctx, &books, query); err != nil {
		return nil, err
	}

	return convertFunc(books), nil
}

func (p *Postgres) DeleteBook(ctx context.Context, id uuid.UUID) error {
	return p.deleteBook(ctx, id)
}

func (p *Postgres) deleteBook(ctx context.Context, id uuid.UUID) error {
	query := `delete from books where id = $1`

	_, err := p.db.ExecContext(ctx, query, id)

	return err
}

func (p *Postgres) UpdateBook(ctx context.Context, b book.Book) (book.Book, error) {
	newB, err := p.updateBook(ctx, b)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return book.Book{}, err
		}
		return book.Book{}, err
	}

	return book.UnmarshalBook(book.UnmarshalBookArgs{
		ID:       newB.ID,
		Name:     newB.Name,
		AuthorId: newB.AuthorID,
	})
}

func (p *Postgres) updateBook(ctx context.Context, b book.Book) (converters.Book, error) {
	query := `update books set name = $1, published_year = $2, author_id = $3, category_id = $4 where id = $5`
	if _, err := p.db.ExecContext(ctx, query, b.Name(), b.AuthorId(), b.ID()); err != nil {
		return converters.Book{}, err
	}

	var newB converters.Book
	query = "select * from books where id = $1"
	if err := p.db.GetContext(ctx, &newB, query, b.ID()); err != nil {
		return converters.Book{}, err
	}

	return newB, nil
}

func (p *Postgres) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := p.getBook(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return book.Book{}, err
		}
		return book.Book{}, err
	}

	return book.UnmarshalBook(book.UnmarshalBookArgs{
		ID:       b.ID,
		Name:     b.Name,
		AuthorId: b.AuthorID,
	})
}

func (p *Postgres) getBook(ctx context.Context, id uuid.UUID) (converters.Book, error) {
	query := `select * from books where id = $1`

	var b converters.Book
	if err := p.db.GetContext(ctx, &b, query, id); err != nil {
		return converters.Book{}, err
	}

	return b, nil
}

func (p *Postgres) CreateAuthor(ctx context.Context, a author.Author) error {
	return p.createAuthor(ctx, a)
}

func (p *Postgres) createAuthor(ctx context.Context, a author.Author) error {
	query := `insert into authors values ($1, $2)`

	_, err := p.db.ExecContext(ctx, query, a.ID(), a.Name())
	return err
}

func (p *Postgres) CreateBook(ctx context.Context, b book.Book) error {
	return p.createBook(ctx, b)
}

func (p *Postgres) createBook(ctx context.Context, b book.Book) error {
	query := `insert into books values ($1, $2, $3)`

	_, err := p.db.ExecContext(ctx, query, b.ID(), b.Name(), b.AuthorId())
	return err
}

func convertFunc(books []converters.Book) []book.Book {
	newBooks := make([]book.Book, 0)
	for _, b := range books {
		newBook, err := book.UnmarshalBook(book.UnmarshalBookArgs{
			ID:       b.ID,
			Name:     b.Name,
			AuthorId: b.AuthorID,
		})
		if err != nil {
			return nil
		}
		newBooks = append(newBooks, newBook)
	}
	return newBooks
}

func (p *Postgres) Cleanup(ctx context.Context) error {
	query := `delete from books`
	_, err := p.db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `delete from authors`
	_, err = p.db.ExecContext(ctx, query)
	if err != nil {
		return err
	}
	return nil
}
