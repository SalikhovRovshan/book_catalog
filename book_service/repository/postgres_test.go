package repository

import (
	"book_service/config"
	"book_service/domain/author"
	"book_service/domain/book"
	"book_service/pkg/converters"
	"context"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"reflect"
	"testing"
)

func TestNewPostgres(t *testing.T) {
	type args struct {
		config config.Postgresconfig
	}
	tests := []struct {
		name    string
		args    args
		want    *Postgres
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewPostgres(tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewPostgres() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPostgres() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_Cleanup(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.Cleanup(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Cleanup() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_CreateAuthor(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		a   author.Author
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.CreateAuthor(tt.args.ctx, tt.args.a); (err != nil) != tt.wantErr {
				t.Errorf("CreateAuthor() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_CreateBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		b   book.Book
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.CreateBook(tt.args.ctx, tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("CreateBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_DeleteBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.DeleteBook(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_GetBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    book.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.GetBook(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_GetListBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []book.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.GetListBook(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_UpdateBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		b   book.Book
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    book.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.UpdateBook(tt.args.ctx, tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_createAuthor(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		a   author.Author
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.createAuthor(tt.args.ctx, tt.args.a); (err != nil) != tt.wantErr {
				t.Errorf("createAuthor() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_createBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		b   book.Book
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.createBook(tt.args.ctx, tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("createBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_deleteBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			if err := p.deleteBook(tt.args.ctx, tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("deleteBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostgres_getBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    converters.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.getBook(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("getBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_getListBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []book.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.getListBook(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("getListBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getListBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_updateBook(t *testing.T) {
	type fields struct {
		db *sqlx.DB
	}
	type args struct {
		ctx context.Context
		b   book.Book
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    converters.Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Postgres{
				db: tt.fields.db,
			}
			got, err := p.updateBook(tt.args.ctx, tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("updateBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("updateBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}
