create table authors(
    id uuid primary key ,
    name varchar(30) not null
);

create table books(
    id uuid primary key ,
    name varchar(30) not null unique ,
    author_id uuid not null references authors(id)
);