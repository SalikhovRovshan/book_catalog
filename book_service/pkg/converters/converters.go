package converters

import (
	"book_service/bookpb"
	"book_service/domain/author"
	"book_service/domain/book"
	"github.com/google/uuid"
)

type Book struct {
	ID       uuid.UUID `db:"id"`
	Name     string    `db:"name"`
	AuthorID uuid.UUID `db:"author_id"`
}

func ToProtoBooks(books []book.Book) *bookpb.Books {
	var protoBooks []*bookpb.Book
	for _, b := range books {
		protoBooks = append(protoBooks, ToProtoBook(b))
	}
	return &bookpb.Books{Books: protoBooks}
}

func ToProtoBook(b book.Book) *bookpb.Book {
	return &bookpb.Book{
		Id:       b.ID().String(),
		Name:     b.Name(),
		AuthorId: b.AuthorId().String(),
	}
}

func ToProtoAuthor(a author.Author) *bookpb.Author {
	return &bookpb.Author{
		Id:   a.ID().String(),
		Name: a.Name(),
	}
}
