package book

import (
	"book_service/pkg/id"
	"github.com/google/uuid"
)

type Factory struct {
	idGenerator id.IDGenerator
}

func NewFactory(idGen id.IDGenerator) Factory {
	return Factory{
		idGenerator: idGen,
	}
}

func (f Factory) NewBook(name string, authorID uuid.UUID) (Book, error) {
	b := Book{
		id:       f.idGenerator.GenerateUUID(),
		name:     name,
		authorID: authorID,
	}

	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}
