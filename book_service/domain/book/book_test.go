package book

import (
	"book_service/pkg/id"
	"github.com/google/uuid"
	"reflect"
	"testing"
)

func TestBook_AuthorId(t *testing.T) {
	type fields struct {
		id       uuid.UUID
		name     string
		authorID uuid.UUID
	}
	tests := []struct {
		name   string
		fields fields
		want   uuid.UUID
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Book{
				id:       tt.fields.id,
				name:     tt.fields.name,
				authorID: tt.fields.authorID,
			}
			if got := b.AuthorId(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AuthorId() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBook_ID(t *testing.T) {
	type fields struct {
		id       uuid.UUID
		name     string
		authorID uuid.UUID
	}
	tests := []struct {
		name   string
		fields fields
		want   uuid.UUID
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Book{
				id:       tt.fields.id,
				name:     tt.fields.name,
				authorID: tt.fields.authorID,
			}
			if got := b.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBook_Name(t *testing.T) {
	type fields struct {
		id       uuid.UUID
		name     string
		authorID uuid.UUID
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Book{
				id:       tt.fields.id,
				name:     tt.fields.name,
				authorID: tt.fields.authorID,
			}
			if got := b.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBook_validate(t *testing.T) {
	type fields struct {
		id       uuid.UUID
		name     string
		authorID uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Book{
				id:       tt.fields.id,
				name:     tt.fields.name,
				authorID: tt.fields.authorID,
			}
			if err := b.validate(); (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFactory_NewBook(t *testing.T) {
	type fields struct {
		idGenerator id.IDGenerator
	}
	type args struct {
		name     string
		authorID uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := Factory{
				idGenerator: tt.fields.idGenerator,
			}
			got, err := f.NewBook(tt.args.name, tt.args.authorID)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewFactory(t *testing.T) {
	type args struct {
		idGen id.IDGenerator
	}
	tests := []struct {
		name string
		args args
		want Factory
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewFactory(tt.args.idGen); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewFactory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnmarshalBook(t *testing.T) {
	type args struct {
		args UnmarshalBookArgs
	}
	tests := []struct {
		name    string
		args    args
		want    Book
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnmarshalBook(tt.args.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnmarshalBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}
