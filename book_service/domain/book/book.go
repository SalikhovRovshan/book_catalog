package book

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var ErrInvalidBookData = errors.New("invalid book data")

type Book struct {
	id       uuid.UUID
	name     string
	authorID uuid.UUID
}

func (b Book) ID() uuid.UUID {
	return b.id
}

func (b Book) Name() string {
	return b.name
}

func (b Book) AuthorId() uuid.UUID {
	return b.authorID
}

func (b Book) validate() error {
	if b.name == "" {
		fmt.Errorf("%w: empty book name", ErrInvalidBookData)
	}
	return nil
}

type UnmarshalBookArgs struct {
	ID       uuid.UUID
	Name     string
	AuthorId uuid.UUID
}

func UnmarshalBook(args UnmarshalBookArgs) (Book, error) {
	b := Book{
		id:       args.ID,
		name:     args.Name,
		authorID: args.AuthorId,
	}
	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}
