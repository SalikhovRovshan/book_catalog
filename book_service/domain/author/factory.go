package author

import "book_service/pkg/id"

type Factory struct {
	idGenerator id.IDGenerator
}

func NewFactory(idGen id.IDGenerator) Factory {
	return Factory{
		idGenerator: idGen,
	}
}

func (f Factory) NewAuthor(name string) (Author, error) {
	a := Author{
		id:   f.idGenerator.GenerateUUID(),
		name: name,
	}

	if err := a.validate(); err != nil {
		return Author{}, err
	}
	return a, nil
}
