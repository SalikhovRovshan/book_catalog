package author

import (
	"book_service/pkg/id"
	"github.com/google/uuid"
	"reflect"
	"testing"
)

func TestAuthor_ID(t *testing.T) {
	type fields struct {
		id   uuid.UUID
		name string
	}
	tests := []struct {
		name   string
		fields fields
		want   uuid.UUID
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := Author{
				id:   tt.fields.id,
				name: tt.fields.name,
			}
			if got := a.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthor_Name(t *testing.T) {
	type fields struct {
		id   uuid.UUID
		name string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := Author{
				id:   tt.fields.id,
				name: tt.fields.name,
			}
			if got := a.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAuthor_validate(t *testing.T) {
	type fields struct {
		id   uuid.UUID
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := Author{
				id:   tt.fields.id,
				name: tt.fields.name,
			}
			if err := a.validate(); (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFactory_NewAuthor(t *testing.T) {
	type fields struct {
		idGenerator id.IDGenerator
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Author
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := Factory{
				idGenerator: tt.fields.idGenerator,
			}
			got, err := f.NewAuthor(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewAuthor() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAuthor() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewFactory(t *testing.T) {
	type args struct {
		idGen id.IDGenerator
	}
	tests := []struct {
		name string
		args args
		want Factory
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewFactory(tt.args.idGen); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewFactory() = %v, want %v", got, tt.want)
			}
		})
	}
}
